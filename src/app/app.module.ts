import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { TeamLeaderboardComponent } from './team-leaderboard/team-leaderboard.component';
import { TeamDetailsModalComponent } from './team-leaderboard/team-details-modal/team-details-modal.component';
import { GlobalReportComponent } from './global-report/global-report.component';
import { DtsAlertComponent } from './alert/dts-alert/dts-alert.component';
import { GoalsComponent } from './goals/goals.component';
import { GoalAlertComponent } from './alert/goal-alert/goal-alert.component';

export const routes: Routes = [
  { path: '', redirectTo: 'leaderboard', pathMatch: 'full' },
  { path: 'leaderboard', component: LeaderboardComponent },
  { path: 'teams', component: TeamLeaderboardComponent },
  { path: 'globalReport', component: GlobalReportComponent },
  { path: 'goals', component: GoalsComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LeaderboardComponent,
    PageNotFoundComponent,
    TeamLeaderboardComponent,
    TeamDetailsModalComponent,
    GlobalReportComponent,
    DtsAlertComponent,
    GoalsComponent,
    GoalAlertComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    NgbModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
