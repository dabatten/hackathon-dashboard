import { Component, OnInit } from '@angular/core';
import { SalesRecordsService } from '../services/sales-records.service';
import { SalesRecord } from '../models/salesRecord';

@Component({
  selector: 'app-global-report',
  templateUrl: './global-report.component.html',
  styleUrls: ['./global-report.component.css']
})
export class GlobalReportComponent implements OnInit {

  constructor(private srService: SalesRecordsService) { }

  allSales: SalesRecord[] = [];

  ngOnInit() {
    this.allSales = this.srService.getAllSalesRecords();

    setInterval(() => {
      this.srService.mutateData(this.allSales);
    }, 3000);
  }

  getSetTotal(sales: SalesRecord[]) {
    let total = 0;
    sales.forEach((sr) => {
      total = +total + +sr.set;
    });
    return total;
  }

  getAttendedTotal(sales: SalesRecord[]) {
    let total = 0;
    sales.forEach((sr) => {
      total = +total + +sr.attended;
    });
    return total;
  }

  getDtsTotal(sales: SalesRecord[]) {
    let total = 0;
    sales.forEach((sr) => {
      total = +total + +sr.dts;
    });
    return total;
  }
}
