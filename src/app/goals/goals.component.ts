import { Component, OnInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { TeamRecord } from '../models/teamRecord';
import { TeamRecordService } from '../services/team-record.service';
import { SalesRecord } from '../models/salesRecord';
import { SalesRecordsService } from '../services/sales-records.service';

@Component({
  selector: 'app-goals',
  templateUrl: './goals.component.html',
  styleUrls: ['./goals.component.css']
})
export class GoalsComponent implements OnInit, AfterViewInit {

  teamRecords: TeamRecord[] = [];
  bdrRecords: SalesRecord[] = [];
  bdsRecords: SalesRecord[] = [];

  constructor(private teamRecordService: TeamRecordService,
    private salesRecordService: SalesRecordsService,
    private ref: ChangeDetectorRef) { }

  ngOnInit() {
    this.teamRecords = this.teamRecordService.getTeamCloses();
    this.bdrRecords = this.salesRecordService.getBDRSalesRecords();
    this.bdsRecords = this.salesRecordService.getBDSSalesRecords();
  }

  ngAfterViewInit() {
    this.ref.detectChanges();
  }

  getTeamProgress(t: TeamRecord): number {
    return t.closes + 100;
  }

  getTeamProgressPercent(t: TeamRecord): number {
    return this.getTeamProgress(t) / 195 * 100;
  }

  getIndividualProgressPercent(s: SalesRecord, goal: number): number {
    return (s.dts / goal) * 100;
  }
}

