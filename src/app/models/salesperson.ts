export class Salesperson {
    name: string;
    team: string;
    role: Role;
}
