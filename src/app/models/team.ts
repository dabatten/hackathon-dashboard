import { Salesperson } from './salesperson';
import { SalesRecord } from './salesRecord';

export class Team {
    members: Salesperson[];
    records: SalesRecord[];
    name: string;
}
