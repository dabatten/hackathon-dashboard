import { Salesperson } from './salesperson';
import { BDR_SET_POINTS, BDR_ATTENDED_POINTS, BDR_DTS_POINTS } from '../const';

export class SalesRecord {
    name: string;
    set: number;
    attended: number;
    dts: number;
    sold: number;
    highlight: boolean;
}
