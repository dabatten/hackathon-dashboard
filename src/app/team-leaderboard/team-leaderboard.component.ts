import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { TeamService } from '../services/team.service';
import { Team } from '../models/team';
import { Salesperson } from '../models/salesperson';
import { SalesRecord } from '../models/salesRecord';
import { TeamDetailsModalComponent } from './team-details-modal/team-details-modal.component';

@Component({
  selector: 'app-team-leaderboard',
  templateUrl: './team-leaderboard.component.html',
  styleUrls: ['./team-leaderboard.component.css']
})
export class TeamLeaderboardComponent implements OnInit {
  @ViewChild('details') modal: TeamDetailsModalComponent;

  constructor(private teamService: TeamService, private ref: ChangeDetectorRef) { }

  teams: Team[] = [];

  ngOnInit() {
    this.teams = this.teamService.getTeams();

    setInterval(() => {
      this.teamService.mutateData(this.teams);
      this.rankTeams();
      this.ref.detectChanges();
    }, 4000);

  }

  getTeamScore(team: Team): number {
    const bdrScore = this.teamService.getTeamRoleScore(1, team);
    const bdsScore = this.teamService.getTeamRoleScore(2, team);
    return Math.ceil((bdrScore + bdsScore) / team.members.length);
  }

  rankTeams() {
    this.teams.sort((a, b) => {
      return this.getTeamScore(b) - this.getTeamScore(a);
    });
  }

}
