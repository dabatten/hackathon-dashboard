import { Component, OnInit, NgModule, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Team } from '../../models/team';
import { Salesperson } from '../../models/salesperson';
import { SalesRecord } from '../../models/salesRecord';
import { BDR_SET_POINTS, BDR_ATTENDED_POINTS, BDR_DTS_POINTS, BDS_SET_POINTS, BDS_ATTENDED_POINTS,
   BDS_DTS_POINTS, BDS_SALES_POINTS } from '../../const';
import { TeamService } from '../../services/team.service';

declare function require(name: string);
const feather = require('feather-icons');

enum Role {
  BDR = 1,
  BDS = 2
}

@Component({
  selector: 'app-team-details-modal',
  templateUrl: './team-details-modal.component.html',
  styleUrls: ['./team-details-modal.component.css']
})

export class TeamDetailsModalComponent {
  @ViewChild('content') content: any;

  team: Team = null;

  /* totals */
  bdrSet = 0;
  bdrAttended = 0;
  bdrDts = 0;
  bdrTotal = 0;

  bdsSet = 0;
  bdsAttended = 0;
  bdsDts = 0;
  bdsSold = 0;
  bdsTotal = 0;

  constructor(private modal: NgbModal, private teamService: TeamService) { }

  open(team: Team) {
    this.team = team;

    this.bdrSet = this.teamService.getTeamRoleSetTotal(Role.BDR, this.team);
    this.bdrAttended = this.teamService.getTeamRoleAttendedTotal(Role.BDR, this.team);
    this.bdrDts = this.teamService.getTeamRoleDtsTotal(Role.BDR, this.team);
    this.bdrTotal = this.teamService.getTeamRoleScore(Role.BDR, this.team);

    this.bdsSet = this.teamService.getTeamRoleSetTotal(Role.BDS, this.team);
    this.bdsAttended = this.teamService.getTeamRoleAttendedTotal(Role.BDS, this.team);
    this.bdsDts = this.teamService.getTeamRoleDtsTotal(Role.BDS, this.team);
    this.bdsSold = this.teamService.getTeamRoleSoldTotal(Role.BDS, this.team);
    this.bdsTotal = this.teamService.getTeamRoleScore(Role.BDS, this.team);

    this.modal.open(this.content, {size: 'lg'}).result.then((res) => {
      console.log(res);
    });
  }

}
