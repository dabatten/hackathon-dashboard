import { Component, OnInit } from '@angular/core';
import { Salesperson } from '../models/salesperson';
import * as d3 from 'd3';
import { SalesRecord } from '../models/salesRecord';
import { SalesRecordsService } from '../services/sales-records.service';
import {
  BDR_SET_POINTS, BDR_ATTENDED_POINTS, BDR_DTS_POINTS, BDS_SALES_POINTS,
  BDS_DTS_POINTS, BDS_ATTENDED_POINTS, BDS_SET_POINTS
} from '../const';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})
export class LeaderboardComponent implements OnInit {

  bdrSalesRecords: SalesRecord[] = [];
  bdsSalesRecords: SalesRecord[] = [];
  bdrSetMvps: SalesRecord[] = [];
  bdsSoldMvps: SalesRecord[] = [];

  /* constants */
  bdrSetPoints = BDR_SET_POINTS;
  bdrAttendedPoints = BDR_ATTENDED_POINTS;
  bdrDTSPoints = BDR_DTS_POINTS;
  bdsSetPoints = BDS_SET_POINTS;
  bdsAttendedPoints = BDS_ATTENDED_POINTS;
  bdsDTSPoints = BDS_DTS_POINTS;
  bdsSalesPoints = BDS_SALES_POINTS;

  constructor(private srService: SalesRecordsService) { }

  ngOnInit() {

    this.bdrSalesRecords = this.srService.getBDRSalesRecords();
    this.bdsSalesRecords = this.srService.getBDSSalesRecords();
    this.sortSalesPeople(this.bdrSalesRecords);
    this.sortSalesPeople(this.bdsSalesRecords);
    this.getBDRSetMVPs();
    this.getBDSSoldMVPs();

    setInterval(() => {
      this.srService.mutateData(this.bdrSalesRecords);
      this.srService.mutateData(this.bdsSalesRecords);
      this.sortSalesPeople(this.bdrSalesRecords);
      this.sortSalesPeople(this.bdsSalesRecords);
      this.getBDRSetMVPs();
      this.getBDSSoldMVPs();
    }, 3000);

  }

  mutateData(salesRecords: SalesRecord[]) {
    const sr = salesRecords[Math.floor(Math.random() * salesRecords.length)];
    const propToChange = Math.floor(Math.random() * 3);
    switch (propToChange) {
      case 0:
        sr.dts++;
        break;
      case 1:
        sr.attended++;
        break;
      case 2:
        sr.set++;
        break;
      case 3:
        sr.sold++;
        break;
      default:
        break;
    }
    salesRecords.forEach((unchanged) => {
      unchanged.highlight = false;
    });
    sr.highlight = true;
  }

  sortSalesPeople(salesRecords: SalesRecord[]) {
    salesRecords.sort((a, b) => {
      const aTotal = (a.set * this.bdrSetPoints) + (a.attended * this.bdrAttendedPoints) + (a.dts * this.bdrDTSPoints);
      const bTotal = (b.set * this.bdrSetPoints) + (b.attended * this.bdrAttendedPoints) + (b.dts * this.bdrDTSPoints);
      if (bTotal === aTotal) {
        if (b.sold === a.sold) {
          if (b.dts === a.dts) {
            if (b.attended === a.attended) {
              return b.set - a.set;
            }
            return b.attended - a.attended;
          }
          return b.dts - a.dts;
        }
        return b.sold - a.sold;
      }
      return bTotal - aTotal;
    });
  }

  getBDRBonus(sr: SalesRecord, rankings: SalesRecord[]): number {
    const topSpots = Math.floor(0.2 * rankings.length);
    if (rankings.indexOf(sr) >= topSpots) {
      return 0;
    }
    switch (rankings.indexOf(sr)) {
      case 0:
        return 600;
      case 1:
        return 500;
      case 2:
        return 400;
      case 3:
        return 300;
      default:
        return 300 - ((rankings.indexOf(sr) - 3) * 25);
    }
  }

  getBDSBonus(sr: SalesRecord, rankings: SalesRecord[]): number {
    const topSpots = Math.floor(0.2 * rankings.length);
    if (rankings.indexOf(sr) > topSpots) {
      return 0;
    }
    return 450 - (rankings.indexOf(sr) * 100);
  }

  getBDRSetMVPs() {
    this.bdrSetMvps = Object.assign([], this.bdrSalesRecords);
    this.bdrSetMvps.sort((a, b) => {
      return +b.set - +a.set;
    });
  }

  getBDSSoldMVPs() {
    this.bdsSoldMvps = Object.assign([], this.bdsSalesRecords);
    this.bdsSoldMvps.sort((a, b) => {
      return +b.sold - +a.sold;
    });
  }

  getBDRSetMVPBonus(index: number) {
    switch (index) {
      case 0:
        return 100;
      case 1:
        return 50;
      default:
        return 0;
    }
  }


}

