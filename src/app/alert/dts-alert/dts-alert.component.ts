import { Component, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dts-alert',
  templateUrl: './dts-alert.component.html',
  styleUrls: ['./dts-alert.component.css']
})
export class DtsAlertComponent {
  @ViewChild('content') content: any;
  name: string;

  constructor(private modal: NgbModal) { }

  show(name: string) {
    this.name = name;
    const modalRef = this.modal.open(this.content, { centered: true, size: 'lg', windowClass: 'full-alert' });

    const audio = document.createElement('audio');
    audio.src = '../../../assets/Ric_Flair___Woo.mp3';
    audio.load();
    audio.play();

    setTimeout(() => modalRef.close(), 5000);
  }

}
