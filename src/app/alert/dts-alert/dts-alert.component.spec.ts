import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtsAlertComponent } from './dts-alert.component';

describe('DtsAlertComponent', () => {
  let component: DtsAlertComponent;
  let fixture: ComponentFixture<DtsAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtsAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtsAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
