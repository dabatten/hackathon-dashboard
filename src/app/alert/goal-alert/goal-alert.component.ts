import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-goal-alert',
  templateUrl: './goal-alert.component.html',
  styleUrls: ['./goal-alert.component.css']
})
export class GoalAlertComponent implements OnInit {
  @ViewChild('content') content: any;
  name: string;

  constructor(private modal: NgbModal) { }

  ngOnInit() {
  }

  show(name: string) {
    this.name = name;
    const modalRef = this.modal.open(this.content, { centered: true, size: 'lg', windowClass: 'full-alert' });

    const audio = document.createElement('audio');
    audio.src = '../../../assets/MLG_Air_Horn_Sound_Effect_FREE.mp3';
    audio.load();
    audio.play();

    setTimeout(() => modalRef.close(), 5000);
  }
}
