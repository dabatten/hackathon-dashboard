export const BDR_SET_POINTS = 100;
export const BDR_ATTENDED_POINTS = 300;
export const BDR_DTS_POINTS = 400;

export const BDS_SET_POINTS = 100;
export const BDS_ATTENDED_POINTS = 300;
export const BDS_DTS_POINTS = 300;
export const BDS_SALES_POINTS = 300;

export const BDS_TEAM_BONUS = 250;
export const BDR_TEAM_BONUS = 225;

declare global {
    enum Role {
        BDR = 1,
        BDS = 2
    }
}
