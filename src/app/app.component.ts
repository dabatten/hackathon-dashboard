import { Component, OnInit, ViewChild } from '@angular/core';
import { DtsAlertComponent } from './alert/dts-alert/dts-alert.component';
import { GoalAlertComponent } from './alert/goal-alert/goal-alert.component';

declare function require(name: string);
const feather = require('feather-icons');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('dtsAlert') dtsAlert: DtsAlertComponent;
  @ViewChild('goalAlert') goalAlert: GoalAlertComponent;
  title = 'app';

  ngOnInit() {
    feather.replace();
  }
}
