import { TestBed, inject } from '@angular/core/testing';

import { TeamRecordService } from './team-record.service';

describe('TeamRecordService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TeamRecordService]
    });
  });

  it('should be created', inject([TeamRecordService], (service: TeamRecordService) => {
    expect(service).toBeTruthy();
  }));
});
