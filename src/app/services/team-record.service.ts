import { Injectable } from '@angular/core';
import { TeamRecord } from '../models/teamRecord';

import * as d3 from 'd3';

@Injectable({
  providedIn: 'root'
})
export class TeamRecordService {

  constructor() { }

  getTeamCloses(): TeamRecord[] {
    const records: TeamRecord[] = [];
    loadTeamCloses(records);
    return records;
  }

}

function loadTeamCloses(data: TeamRecord[]) {
  return d3.json('https://spreadsheets.google.com/feeds/list/1mS5WRhRKb5H1GXL1TE_PDWuphQ6x-Igmi0fmSPj4EJ8/1/public/values?alt=json')
    .then(function (table: any) {
      const entry = table.feed.entry;
      entry.forEach(function (val, index) {
        data.push({
          teamName: getValue(val.gsx$teamname) || '???',
          closes: +getValue(val.gsx$closes) || 0,
        });
      });
    });
}

function getValue(field) {
  if (field && field.$t) {
    return field.$t;
  }
  return undefined;
}
