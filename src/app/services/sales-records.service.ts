import { Injectable } from '@angular/core';
import * as d3 from 'd3';
import { SalesRecord } from '../models/salesRecord';

@Injectable({
  providedIn: 'root'
})
export class SalesRecordsService {
  getBDSSalesRecords(): SalesRecord[] {
    const bdsSales: SalesRecord[] = [];
    loadBDSData(bdsSales);
    return bdsSales;
  }
  getBDRSalesRecords(): SalesRecord[] {
    const bdrSales: SalesRecord[] = [];
    loadBDRData(bdrSales);
    return bdrSales;
  }
  getAllSalesRecords(): SalesRecord[] {
    const allSales: SalesRecord[] = [];
    loadBDRData(allSales);
    loadBDSData(allSales);
    return allSales;
  }

  mutateData(salesRecords: SalesRecord[]) {
    const sr = salesRecords[Math.floor(Math.random() * salesRecords.length)];
    const propToChange = Math.floor(Math.random() * 3);
    switch (propToChange) {
      case 0:
        sr.dts++;
        break;
      case 1:
        sr.attended++;
        break;
      case 2:
        sr.set++;
        break;
      case 3:
        sr.sold++;
        break;
      default:
        break;
    }
    salesRecords.forEach((unchanged) => {
      unchanged.highlight = false;
    });
    sr.highlight = true;
  }
}

function loadBDSData(data: SalesRecord[]) {
  return d3.json('https://spreadsheets.google.com/feeds/list/11mOSk8sPScrmpq5XLPGBkpE0697g_WEhFlCTx-z0zrk/1/public/values?alt=json')
  .then(function (table: any) {
    const entry = table.feed.entry;
    entry.forEach(function(val, index) {
      data.push({
        attended: +getValue(val.gsx$attended) || 0,
        dts: +getValue(val.gsx$dts) || 0,
        name: getValue(val.gsx$name) || '???',
        set: +getValue(val.gsx$set) || 0,
        sold: +getValue(val.gsx$sold) || 0,
        highlight: false
      });
    });
  });
}

function loadBDRData(data: SalesRecord[]) {
  return d3.json('https://spreadsheets.google.com/feeds/list/1VduW14OVkIFPSv5yqvcBDpG9m69pYYO-KiM-h2rLLq4/1/public/values?alt=json')
  .then(function (table: any) {
    const entry = table.feed.entry;
    entry.forEach(function(val, index) {
      data.push({
        attended: +getValue(val.gsx$attended) || 0,
        dts: +getValue(val.gsx$dts) || 0,
        name: getValue(val.gsx$_cn6ca) || '???',
        set: +getValue(val.gsx$set) || 0,
        sold: +getValue(val.gsx$sold) || 0,
        highlight: false
      });
    });
  });
}

function getValue(field) {
  if (field && field.$t) {
    return field.$t;
  }
  return undefined;
}
