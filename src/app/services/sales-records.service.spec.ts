import { TestBed, inject } from '@angular/core/testing';

import { SalesRecordsService } from './sales-records.service';

describe('SalesRecordsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SalesRecordsService]
    });
  });

  it('should be created', inject([SalesRecordsService], (service: SalesRecordsService) => {
    expect(service).toBeTruthy();
  }));
});
