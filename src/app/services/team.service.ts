import { Injectable } from '@angular/core';
import { Salesperson } from '../models/salesperson';
import { Team } from '../models/team';
import { getQueryValue } from '@angular/core/src/view/query';
import * as d3 from 'd3';
import { SafePropertyRead } from '@angular/compiler';
import { SalesRecordsService } from './sales-records.service';
import { SalesRecord } from '../models/salesRecord';

import { BDR_SET_POINTS, BDR_ATTENDED_POINTS, BDR_DTS_POINTS, BDS_ATTENDED_POINTS, BDS_SET_POINTS,
   BDS_DTS_POINTS, BDS_SALES_POINTS } from '../const';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(private salesRecordService: SalesRecordsService) {
  }

  getTeams(): Team[] {
    const teams: Team[] = [];
    let teamMap: Map<string, Salesperson[]> = new Map();

    const allSalespeople: Salesperson[] = [];

    const records: SalesRecord[] = this.salesRecordService.getAllSalesRecords();
    loadBDSs(allSalespeople).then(() => {
      loadBDRs(allSalespeople).then(() => {
        teamMap = groupBy(allSalespeople, sp => sp.team);
        teamMap.forEach((salespeople, teamName) => {
          teams.push({
            name: teamName,
            members: salespeople,
            records: records.filter((r) => {
              let include = false;
              salespeople.forEach((s) => {
                if (s.name === r.name) {
                  include = true;
                }
              });
              return include;
            })
          });
        });
        console.log(teams);
      });
    });
    return teams;
  }


  mutateData(teams: Team[]) {

    const t = teams[Math.floor(Math.random() * teams.length)];
    const sales = t.records.map(x => Object.assign({}, x));
    this.salesRecordService.mutateData(sales);
    this.salesRecordService.mutateData(sales);
    this.salesRecordService.mutateData(sales);
    t.records = sales;
  }

  getTeamRoleSetTotal(role: number, team: Team): number {
    const records = getRecordsForRole(role, team);
    let total = 0;
    records.forEach((r) => {
      total = total + +r.set;
    });
    return total;
  }

  getTeamRoleAttendedTotal(role: number, team: Team): number {
    const records = getRecordsForRole(role, team);
    let total = 0;
    records.forEach((r) => {
      total += +r.attended;
    });
    return total;
  }

  getTeamRoleDtsTotal(role: number, team: Team): number {
    const records = getRecordsForRole(role, team);
    let total = 0;
    records.forEach((r) => {
      total += +r.dts;
    });
    return total;
  }

  getTeamRoleSoldTotal(role: number, team: Team): number {
    const records = getRecordsForRole(role, team);
    let total = 0;
    records.forEach((r) => {
      total += +r.sold;
    });
    return total;
  }

  getTeamRoleScore(role: number, team: Team): number {
    const set = this.getTeamRoleSetTotal(role, team);
    const attended = this.getTeamRoleSetTotal(role, team);
    const dts = this.getTeamRoleSetTotal(role, team);
    const sold = this.getTeamRoleSetTotal(role, team);
    if (+role === 1) {
      return set * BDR_SET_POINTS + attended * BDR_ATTENDED_POINTS + dts * BDR_DTS_POINTS;
    } else {
      return set * BDS_SET_POINTS + attended * BDS_ATTENDED_POINTS + dts * BDS_DTS_POINTS
        + sold * BDS_SALES_POINTS;
    }
  }
}

function loadBDRs(data: Salesperson[]) {
  return d3.json('https://spreadsheets.google.com/feeds/list/1sqcXYpsd3Mo3_f4CXn9EUPm38BB1VCfhpAno1J_zW3A/1/public/values?alt=json')
    .then(function (table: any) {
      const entry = table.feed.entry;
      entry.forEach(function (val, index) {
        data.push({
          name: getValue(val.gsx$name) || '???',
          team: getValue(val.gsx$team) || '???',
          role: 1
        });
      });
    });
}

function loadBDSs(data: Salesperson[]) {
  return d3.json('https://spreadsheets.google.com/feeds/list/1n2_xkWaRfFs4kYFyRGS9ZLvul5g8MpK3BjXUSKUy6Iw/1/public/values?alt=json')
    .then(function (table: any) {
      const entry = table.feed.entry;
      entry.forEach(function (val, index) {
        data.push({
          name: getValue(val.gsx$name) || '???',
          team: getValue(val.gsx$team) || '???',
          role: 2
        });
      });
    });
}

function getValue(field) {
  if (field && field.$t) {
    return field.$t;
  }
  return undefined;
}

function groupBy(list, keyGetter) {
  const map = new Map();
  list.forEach((item) => {
    const key = keyGetter(item);
    const collection = map.get(key);
    if (!collection) {
      map.set(key, [item]);
    } else {
      collection.push(item);
    }
  });
  return map;
}

function getRecordsForRole(role: number, team: Team): SalesRecord[] {
  const members: Salesperson[] = team.members.filter((m) => {
    return +m.role === +role;
  });
  const records: SalesRecord[] = team.records.filter((r) => {
    let include = false;
    members.forEach((m) => {
      if (m.name === r.name) {
        include = true;
      }
    });
    return include;
  });
  return records;
}
